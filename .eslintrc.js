// eslint-disable-next-line no-undef
module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['@typescript-eslint', 'react', 'react-hooks'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
  ],
  rules: {
    'no-cond-assign': 'error',
    'no-constant-condition': 'error',
    'no-console': 'error',
    'constructor-super': 'error',
    'no-dupe-keys': 'error',
    'no-duplicate-case': 'error',
    'no-empty': 'error',
    'no-empty-function': 'error',
    'no-invalid-regexp': 'error',
    'no-this-before-super': 'error',
    'no-undef': 'error',
    'no-unused-expressions': 'error',
    'no-unused-vars': 'error',
    'no-use-before-define': 'error',
    'complexity': ['error', 10],
    'max-len': ['error', { 'code': 80 }],
    'indent': ['error', 2, { 'SwitchCase': 1 }],
    'no-multi-spaces': 'error',
    'eol-last': 'error',
    'quotes': ['error', 'single'],
    'semi': 'error',
  }
      
};
  
