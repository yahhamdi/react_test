import React from 'react';
import Dashboard from './components/Dashboard/Dashboard';
import PreSelection from './components/Preselection/PreSelection';
import Selection from './components/Selection/Selection';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

const App: React.FC = () => {

  return (
    <div className="container">
      <div className="row">
        <Dashboard />

        <PreSelection />

        <Selection />
      </div>
    </div>
  );
};

export default App;
