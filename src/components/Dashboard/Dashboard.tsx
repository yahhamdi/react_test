import React from 'react';
import './Dashboard.css';

const Dashboard = () => {
  return (
    <div className="col-2">
      <div className="nav flex-column nav-pills" id="v-pills-tab">
        <button className="nav-link selected" id="v-pills-projects-tab">
          <span className="label">Projets</span>
          <span className="arrow-icon">-&gt;</span>
        </button>
        <button className="nav-link not-selected" id="v-pills-affairs-tab">
          <span className="label">Affaires</span>
          <span className="arrow-icon">-&gt;</span>
        </button>
      </div>
    </div>
  );
};

export default Dashboard;
