/* eslint-disable no-unused-vars */
import React from 'react';
import { FaPlus } from 'react-icons/fa';
import { ProjectModel } from '../../models/ProjectModel';
import { AffairModel } from '../../models/AffairModel';
import SearchInput from '../SearchInput/SearchInput';
import { projects } from '../../data/data';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../store/constants/State';
import { selectProject } from '../../store/actions/selectProjectActions';
import { selectAffair } from '../../store/actions/selectAffairActions';
import './PreSelection.css';


const PreSelection = () => {
  // eslint-disable-next-line max-len
  const selectedProjects = useSelector((state : State) => state.selectedProjects);
  const selectedAffairs = useSelector((state : State) => state.selectedAffairs);
  const dispatch = useDispatch();

  const handleProjectSelection = (project : ProjectModel) => {
    dispatch(selectProject(project));
  };

  const handleAffairSelection = (affair : AffairModel) => {
    dispatch(selectAffair(affair));
  };

  return (
    <div className="col-5">
      <SearchInput />
      <div className="tab-content" id="v-pills-tabContent">
        <div className="tab-pane fade show active" id="v-pills-projects">
          <ul className="list-group list-group-flush">
            {projects.map((project) => (
              <li key={project.id} className="list-group-item border-0">
                <div className="form-check">
                  <input
                    type="checkbox"
                    checked={selectedProjects.includes(project)}
                    onChange={() => handleProjectSelection(project)}
                    id={project.name}
                    className="btn-check"
                  />
                  <label htmlFor={project.name} className="label-container">
                    <div className="circle-icon">
                      <FaPlus className="icon" />
                    </div>
                    <span className="label-text">{project.name}</span>
                  </label>

                </div>
                {selectedProjects.includes(project) && (
                  <ul className="list-group list-group-flush">
                    {project.affairs?.map((affair) => (
                      <li key={affair.id} className="list-group-item border-0">
                        <div className="form-check">
                          <input
                            type="checkbox"
                            className="btn-check"
                            checked={selectedAffairs.includes(affair)}
                            onChange={() => handleAffairSelection(affair)}
                            id={affair.name}
                          />
                          <label
                            htmlFor={affair.name}
                            className="form-check-label"
                          >
                            {affair.name}
                          </label>
                        </div>
                      </li>
                    ))}
                  </ul>
                )}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default PreSelection;
