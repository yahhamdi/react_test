import React from 'react';
import { FaSearch } from 'react-icons/fa';
import './SearchInput.css';

const SearchInput = () => {
  return (
    <div className="search-container">
      <input type="text" placeholder="Recherche" className="search-input" />
      <FaSearch className="search-icon" />
    </div>
  );
};

export default SearchInput;
