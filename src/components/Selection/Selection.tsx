import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { confirmSelection } from '../../store/actions/confirmSelectionActions';
import { State } from '../../store/constants/State';



const Selection = () => {
  // eslint-disable-next-line max-len
  const selectedProjects = useSelector((state: State) => state.selectedProjects);
  const selectedAffairs = useSelector((state: State) => state.selectedAffairs);
  const dispatch = useDispatch();

  const handleConfirmation = () => {
    dispatch(confirmSelection());
  };

  return (
    <div className="col-4">
      <div className="summary">
        <h3 className="label-selection">SELECTION :</h3>
        {selectedProjects.length > 0 ? (
          <>
            <p>{selectedProjects.length} Projets</p>
            <ul>
              {selectedProjects.map((project) => (
                <li key={project.id}>{project.name}</li>
              ))}
            </ul>
          </>
        ) : null}

        {selectedAffairs.length > 0 ? (
          <>
            <p>{selectedAffairs.length} Affaire</p>
            <ul>
              {selectedAffairs.map((affair) => (
                <li key={affair.id}>{affair.name}</li>
              ))}
            </ul>
          </>
        ) : null}

        <button className="button" onClick={handleConfirmation}>
          Confirmer
        </button>
      </div>
    </div>
  );
};

export default Selection;
