import { AffairModel } from '../../models/AffairModel';

export const selectAffair = (affair: AffairModel) => {
  return {
    type: 'SELECT_AFFAIR',
    payload: affair,
  };
};
