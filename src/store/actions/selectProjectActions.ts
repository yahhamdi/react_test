import { ProjectModel } from '../../models/ProjectModel';


export const selectProject = (project: ProjectModel) => {
  return {
    type: 'SELECT_PROJECT',
    payload: project,
  };
};
