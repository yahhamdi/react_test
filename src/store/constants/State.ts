import { ProjectModel } from '../../models/ProjectModel';
import { AffairModel } from '../../models/AffairModel';


export interface State {
    selectedProjects: ProjectModel[];
    selectedAffairs: AffairModel[];
  }
