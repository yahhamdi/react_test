import { State} from './State';

export const initialState: State = {
  selectedProjects: [],
  selectedAffairs: [],
};
