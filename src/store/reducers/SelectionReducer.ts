import { State } from '../constants/State';
import { initialState } from '../constants/initialState';  
import { Action } from '../constants/Action';

const selectionReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case 'SELECT_PROJECT': {
      const selectedProjects = state.selectedProjects.includes(action.payload)
        ? state.selectedProjects.filter((p) => p !== action.payload)
        : [...state.selectedProjects, action.payload];
      return { ...state, selectedProjects };
    }
    case 'SELECT_AFFAIR': {
      const selectedAffairs = state.selectedAffairs.includes(action.payload)
        ? state.selectedAffairs.filter((a) => a !== action.payload)
        : [...state.selectedAffairs, action.payload];
      return { ...state, selectedAffairs };
    }
    case 'CONFIRM_SELECTION': {
      const selection = {
        selectedProjects: state.selectedProjects.map((project) => project.name),
        selectedAffairs: state.selectedAffairs.map((affair) => affair.name),
      };
        // eslint-disable-next-line no-console, no-undef
      console.log(selection);
      return state;
    }
    default:
      return state;
  }
};


export default selectionReducer;
