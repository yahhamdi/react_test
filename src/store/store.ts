import { createStore } from 'redux';
import selectionReducer from './reducers/SelectionReducer';

const store = createStore(selectionReducer);

export default store;
