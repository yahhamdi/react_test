
/* eslint-disable max-len */
/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import store from '../store/store';
import App from '../App';

describe('Dashboard', ()=>{
  it('displays projects and affairs', () => {
    const { getByText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );
    
    expect(getByText('Projets')).toBeInTheDocument();
    expect(getByText('Affaires')).toBeInTheDocument();
  });
    
});
