/* eslint-disable max-len */
/* eslint-disable no-undef */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '../store/store';
import App from '../App';

describe('Select', () => {
  it('allows selecting projects and affairs', () => {
    const { getByText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    const findElementByText = (text: string): Element | null => {
      const elements = Array.from(document.querySelectorAll('*')).filter(
        (el) => el.textContent === text
      );
      return elements.length ? elements[0] : null;
    };

    const projectCheckbox = getByText('Projet 1').previousSibling as HTMLInputElement;
    const affairElement = findElementByText('Affaire 1.1');
    const affairCheckbox = affairElement?.previousSibling as HTMLInputElement;

    if (affairElement && affairCheckbox) {
      fireEvent.click(projectCheckbox);
      fireEvent.click(affairCheckbox);

      const summaryButton = getByText('Confirmer');
      fireEvent.click(summaryButton);
    }
  });
});
